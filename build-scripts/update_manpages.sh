#!/bin/bash -e
source build-scripts/env.sh

set -x

readonly SUMMARY=`cat summary`

SED_SEARCH="version .*\\\".*$"
SED_REPLACE="version ${VERSION}\\\" \\\"${SUMMARY}\\\""
SED_OPT_MAN="s/${SED_SEARCH}/${SED_REPLACE}/g"

sed -i "${SED_OPT_MAN}" help/${APP}.1
sed -i "${SED_OPT_MAN}" help/${APP}.ru.1
sed -i "${SED_OPT_MAN}" help/${APP}.eo.1

set +x
