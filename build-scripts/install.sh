#!/bin/bash -e
source build-scripts/env.sh
source build-scripts/settings_of_installation

set -x

MODE="$1"
PREFIX="$2"

if [[ ${PREFIX} != "" ]]; then
    BASE="${PREFIX}"
else
    BASE="/usr/local"
fi


read -ra distr_id_arr <<< "`lsb_release -i`"
# lsb_release -i (for example): "Distributor ID: openSUSE"
DISTRIBUTION=${distr_id_arr[2]}  # for example, "openSUSE" or "Debian"
# Exception to the rules
if [[ "${DISTRIBUTION}" == "openSUSE" ]]; then
    DOCDIR="${DOCDIR_OPENSUSE}"
fi


# Directories

INST_BINDIR="${BASE}/${BINDIR}"
INST_MANDIR_EN="${BASE}/${MANDIR_EN}"
INST_MANDIR_RU="${BASE}/${MANDIR_RU}"
INST_MANDIR_EO="${BASE}/${MANDIR_EO}"
INST_BASHCOMPDIR="${BASE}/${BASHCOMP}"

INST_DOCDIR="${BASE}/${DOCDIR}/${APP}"
INST_HELPDIR_EN="${BASE}/${HELPDIR_EN}/${APP}"
INST_HELPDIR_RU="${BASE}/${HELPDIR_RU}/${APP}"
INST_HELPDIR_EO="${BASE}/${HELPDIR_EO}/${APP}"


# Installation

set -x

echo "PREFIX:" ${BASE}

if [[ ${MODE} == "--install" ]]; then
    # prepare directories
    mkdir -p "${INST_BINDIR}" "${INST_BASHCOMPDIR}" "${INST_DOCDIR}"
    mkdir -p "${INST_HELPDIR_EN}" "${INST_HELPDIR_RU}" "${INST_HELPDIR_EO}"
    mkdir -p "${INST_MANDIR_EN}" "${INST_MANDIR_RU}" "${INST_MANDIR_EO}"

    # install the executable file and the bash completion script
    install "${BINPATH}" "${INST_BINDIR}/"
    if [[ -e source/_${APP} ]]; then  # bash completion script exists
        cp source/_${APP} "${INST_BASHCOMPDIR}/${APP}"
    fi

    # copy copyright file
    cp copyright ${INST_DOCDIR}/

    # install help files
    cp help/help_en_US.txt ${INST_HELPDIR_EN}/help.txt
    cp help/help_ru_RU.txt ${INST_HELPDIR_RU}/help.txt
    cp help/help_eo.txt    ${INST_HELPDIR_EO}/help.txt

    # install man-pages
    gzip -9 -k -n help/${APP}.1
    gzip -9 -k -n help/${APP}.ru.1
    gzip -9 -k -n help/${APP}.eo.1
    install help/${APP}.1.gz    ${INST_MANDIR_EN}/${APP}.1.gz
    install help/${APP}.ru.1.gz ${INST_MANDIR_RU}/${APP}.1.gz
    install help/${APP}.eo.1.gz ${INST_MANDIR_EO}/${APP}.1.gz
    rm help/*.1.gz
else
    rm -f ${INST_BINDIR}/${APP}
    rm -f ${INST_BASHCOMPDIR}/${APP}
    rm -rf ${INST_DOCDIR}
    rm -rf ${INST_HELPDIR_EN}
    rm -rf ${INST_HELPDIR_RU}
    rm -rf ${INST_HELPDIR_EO}
    rm -f ${INST_MANDIR_EN}/${APP}.1.gz
    rm -f ${INST_MANDIR_RU}/${APP}.1.gz
    rm -f ${INST_MANDIR_EO}/${APP}.1.gz
fi


set +x
